import { StyleSheet } from 'react-native'
import { Metrics, Colors} from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  UIbutton: {
    marginHorizontal:Metrics.marginHorizontal,
  }
})
