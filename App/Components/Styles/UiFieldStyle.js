import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    borderWidth:1,
    borderColor:Colors.silver,
    borderRadius:50,
  },
  textInput: {
    flex:1,
    color:Colors.snow,
    height:40,
    padding:0,
    paddingRight:10,
    paddingLeft:10
  },
  selectText:{
    color: Colors.snow
  }
})
