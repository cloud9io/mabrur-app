import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  customFAB: {
     position:'absolute',
     right:0,
     bottom:0,
     padding:30,
    backgroundColor:Colors.snow
  }
})
