import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom:5
  },
  imageLabel: {
    marginHorizontal: 15,
    marginVertical:0,
    marginTop:5
  },
  textLabel:{
    color:Colors.snow,
    fontSize: 20,
  }
})
