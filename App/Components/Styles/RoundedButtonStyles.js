import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  button: {
    height: 45,
    borderRadius: 50,
    marginHorizontal: Metrics.section,
    marginVertical: Metrics.baseMargin,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: Colors.fire,
    backgroundColor:Colors.snow
  },
  buttonFill: {
    backgroundColor: Colors.fire,
  },
  buttonText: {
    color: Colors.fire,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.medium,
    marginVertical: Metrics.baseMargin
  },
  buttonTextFill:{
    color: Colors.snow,
  },
  buttonIcon: {
    marginHorizontal: 5,
  },

})
