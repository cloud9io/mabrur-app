import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text , Image } from 'react-native'
import styles from './Styles/UiFieldLabelStyle'
import { Colors } from '../Themes';

export default class UiFieldLabel extends Component {
  // // Prop type warnings
  static propTypes = {
    text: PropTypes.string
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={styles.container}>
        {this.props.image && <Image style={styles.imageLabel} source={this.props.image} tintColor={Colors.snow} />}
        <Text style={styles.textLabel} >{this.props.text}</Text>
      </View>
    )
  }
}
