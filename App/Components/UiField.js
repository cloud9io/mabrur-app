import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, TextInput , Picker } from 'react-native'
import styles from './Styles/UiFieldStyle'
import {Select} from "react-native-chooser";

export default class UiField extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }


  constructor(props) {
    super(props);
    this.state = {value : "Select Me Please"}
  }
  onSelect(value, label) {
    this.setState({value : value});
    this.props.selectChange(value)
  }


  processChange = (text) => {
      this.props.onChange(text);
  }




  render () {
    const inputType = this.props.isPicker ? false : true;
    return (
      <View style={styles.container}>
        {inputType &&
          <TextInput
          {...this.props}
          {...this.props.input}
          onChangeText={this.processChange}
          style={styles.textInput}
          multiline={false}
          underlineColorAndroid={'transparent'}/>}
          {!inputType &&
            <Select
            {...this.props}
            onSelect = {this.onSelect.bind(this)}
            defaultText  = {this.state.value}
            style = {{borderWidth : 0, borderColor : "green" , flex:1 , width:500}}
            textStyle = {styles.selectText}
            backdropStyle  = {{backgroundColor : 'rgba(0, 0, 0, 0.4)'}}
            optionListStyle = {{backgroundColor : "#F5FCFF", height:500}}
          >
            {this.props.children}

        </Select>}


      </View>
    )
  }
}
