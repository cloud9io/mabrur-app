import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text , Image } from 'react-native'
import styles from './Styles/RoundedButtonStyles'
import ExamplesRegistry from '../Services/ExamplesRegistry'
import {  Colors } from '../Themes/'

// Note that this file (App/Components/RoundedButton) needs to be
// imported in your app somewhere, otherwise your component won't be
// compiled and added to the examples dev screen.

// Ignore in coverage report
/* istanbul ignore next */
ExamplesRegistry.addComponentExample('Rounded Button', () =>
  <RoundedButton
    text='real buttons have curves'
    onPress={() => window.alert('Rounded Button Pressed!')}
  />
)

export default class RoundedButton extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    text: PropTypes.string,
    children: PropTypes.string,
    navigator: PropTypes.object,
    transparent: PropTypes.bool
  }

  getText () {
    const buttonText = this.props.text || this.props.children || ''
    return buttonText.toUpperCase()
  }

  render () {
    return (
      <TouchableOpacity style={this.props.transparent ? styles.button : [styles.button , styles.buttonFill] } onPress={this.props.onPress}>

        {this.props.icon && <Image tintColor={this.props.transparent? Colors.fire : Colors.snow} style={styles.buttonIcon} source={this.props.icon} />}

        <Text style={this.props.transparent ? styles.buttonText : [styles.buttonText , styles.buttonTextFill]}>
          {this.getText()}
        </Text>

      </TouchableOpacity>
    )
  }
}
