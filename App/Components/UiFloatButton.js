import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { TouchableOpacity, Text } from 'react-native'
import styles from './Styles/UiFloatButtonStyle'

export default class UiFloatButton extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
        <TouchableOpacity
        {...this.props}
        style={styles.customFAB}
        onPress={this.props.onPress}
        >
          <Text>{this.props.text}</Text>
        </TouchableOpacity>
    )
  }
}
