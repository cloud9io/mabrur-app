import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { Text, TouchableOpacity } from 'react-native'
import styles from './Styles/UIbuttonStyle'

export default class UIbutton extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <TouchableOpacity
      onPress={() => this.props.onPress}
      style={[styles.container , styles.UIbutton]}>
        <Text>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}
