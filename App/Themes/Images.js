// leave off @2x/@3x
const images = {
  logo: require('../Images/logo.png'),
  clearLogo: require('../Images/top_logo.png'),
  launch: require('../Images/launch-icon.png'),
  ready: require('../Images/your-app.png'),
  ignite: require('../Images/ignite_logo.png'),
  igniteClear: require('../Images/ignite-logo-transparent.png'),
  tileBg: require('../Images/tile_bg.png'),
  background: require('../Images/BG.png'),
  buttonBackground: require('../Images/button-bg.png'),
  api: require('../Images/Icons/icon-api-testing.png'),
  components: require('../Images/Icons/icon-components.png'),
  deviceInfo: require('../Images/Icons/icon-device-information.png'),
  faq: require('../Images/Icons/faq-icon.png'),
  home: require('../Images/Icons/icon-home.png'),
  theme: require('../Images/Icons/icon-theme.png'),
  usageExamples: require('../Images/Icons/icon-usage-examples.png'),
  chevronRight: require('../Images/Icons/chevron-right.png'),
  hamburger: require('../Images/Icons/hamburger.png'),
  backButton: require('../Images/Icons/back-button.png'),
  closeButton: require('../Images/Icons/close-button.png'),
  backdrop: require('../Images/backdrop.png'),
  ic_person_add: require("../Images/Icons/ic_person_add.png"),
  ic_person: require("../Images/Icons/ic_person.png"),
  ic_phone: require("../Images/Icons/ic_phone.png"),
  ic_person_outline: require("../Images/Icons/ic_person_outline.png"),
  ic_email: require("../Images/Icons/ic_email.png"),
  ic_lock: require("../Images/Icons/ic_lock.png"),
  ic_world: require("../Images/Icons/ic_world.png"),
  ic_lang: require("../Images/Icons/ic_lang.png"),

  ic_one: require("../Images/Icons/ic_one.png"),
  ic_two: require("../Images/Icons/icon_two.png"),
  ic_three: require("../Images/Icons/icon_three.png"),
  ic_four: require("../Images/Icons/icon_four.png"),

  screen_find: require("../Images/Icons/graph_finding.png")
}

export default images
