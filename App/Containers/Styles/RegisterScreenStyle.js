import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

export default {
  ...ApplicationStyles.screen,
  container: {
    flex:1,
    backgroundColor: Colors.white,
    position: 'relative',

  },
  formInput:{
    marginTop:30
  }
}
