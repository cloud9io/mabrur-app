import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/'

export default {
  ...ApplicationStyles.screen,
  logoConatiner: {
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

  },
  buttonsContainer: {
    flex:1,
  },
  logo: {
    height:150,
    width:150
  }
}
