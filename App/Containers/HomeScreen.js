import React, { Component } from 'react'
import { Text, View , Image , TouchableOpacity} from 'react-native'
import { connect } from 'react-redux'
import { Images, Colors } from '../Themes';
import { Actions } from 'react-native-router-flux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/HomeScreenStyle'

class HomeScreen extends Component {
  render () {
    return (
      <View style={{flex:1}}>
        <View style={{flex:2 , alignItems:"center" , backgroundColor:"#0E5456"}}>
            <Image style={{height:160, width:160, margin:50}} source={Images.logo} />
        </View>
        <View style={{flex:2 , alignItems:"flex-start", flexDirection:"row" , backgroundColor:Colors.snow}}>
            <TouchableOpacity
              style={{flex:1, flexDirection:"column" , alignItems:"center"}}
              onPress={() => Actions.findbot()}
              >
              <Image  style={{height:80, width:80, margin:50}} source={Images.ic_one} />
              <Text style={{color:Colors.white}}>{"Im Sick"}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{flex:1, flexDirection:"column" , alignItems:"center"}}>

              <Image  style={{height:80, width:80, margin:50}} source={Images.ic_two} />
              <Text style={{color:Colors.white}}>{"Talk To Doctor"}</Text>
            </TouchableOpacity>
        </View>
        <View style={{flex:2 , alignItems:"flex-start", flexDirection:"row" , backgroundColor:Colors.snow}}>
          <TouchableOpacity
            style={{flex:1, flexDirection:"column" , alignItems:"center"}}>

            <Image style={{height:80, width:80, margin:50}} source={Images.ic_three} />
            <Text style={{color:Colors.white}}>{"Find My Mate"}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{flex:1, flexDirection:"column" , alignItems:"center"}}>

            <Image style={{height:80, width:80, margin:50}} source={Images.ic_four} />
            <Text style={{color:Colors.white}}>{"Sos"}</Text>
          </TouchableOpacity>
      </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
