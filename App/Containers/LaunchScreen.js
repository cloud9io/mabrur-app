import React, { Component } from 'react'
import { ImageBackground, View , Image} from 'react-native'
import RoundedButton from '../Components/RoundedButton'
import { Images } from '../Themes'
import { Actions } from 'react-native-router-flux';

// Styles
import styles from './Styles/LaunchScreenStyles'


export default class LaunchScreen extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground source={Images.backdrop} style={styles.backgroundImage} >
        <View style={styles.logoConatiner}>
          <Image style={styles.logo} source={Images.logo} />
        </View>
        <View style={styles.buttonsContainer}>
          <RoundedButton
            text='REGISTER'
            onPress={() => Actions.register()}
            icon={Images.ic_person_add}
            transparent={false}
          />
          <RoundedButton
            text='LOG IN'
            onPress={() => Actions.login()}
            icon={Images.ic_person}
            transparent={true}
          />
        </View>
        </ImageBackground>
      </View>
    )
  }
}
