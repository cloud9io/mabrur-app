import React, { Component } from 'react'
import { ImageBackground} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/FindBotScreenStyle'
import { Images } from '../Themes';

class FindBotScreen extends Component {
  render () {
    return (
      <ImageBackground source={Images.screen_find} style={{
        flex:1,
        justifyContent: 'center',
        width: '100%', height: '100%'
      }} >


      </ImageBackground>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FindBotScreen)
