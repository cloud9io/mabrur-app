import React from 'react';
import { Scene, Lightbox, ActionConst, Actions, Stack } from 'react-native-router-flux';
import LaunchScreen from './LaunchScreen';
import RegisterScreen from './RegisterScreen';
import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import FindBot from "./FindBotScreen";


export default Actions.create(
  <Lightbox>
  <Scene
  key='root'
  >
    <Scene
      key={'launch'}
      component={LaunchScreen}
      initial
      type={ActionConst.RESET}
      hideNavBar
    />
    <Scene
      key={'register'}
      component={RegisterScreen}
      title={'Register'}
    />
    <Scene
      key={'login'}
      component={LoginScreen}
      title={'Login'}
    />
    <Scene
      key={'home'}
      component={HomeScreen}
      title={'Home'}
      panHandlers={null}
      hideNavBar
    />
    <Scene
      key={'findbot'}
      component={FindBot}
      title={'Searching'}
      panHandlers={null}
      hideNavBar
    />
  </Scene>
  </Lightbox>
)

