import React , {Component} from 'react'
import { ScrollView, View, KeyboardAvoidingView , TouchableOpacity , Text} from 'react-native'
import { connect } from 'react-redux'
import { Option} from "react-native-chooser";
import RadioGroup from 'react-native-radio-buttons-group';
import UiField from '../Components/UiField';
import UiFieldLabel from '../Components/UiFieldLabel';
import { Images, Colors } from '../Themes';
import configs from '../Config/AppConfig';
import RoundedButton from '../Components/RoundedButton'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import RegisterAction from '../Redux/RegisterFormRedux';

// Styles
import styles from './Styles/RegisterScreenStyle'

class RegisterScreen extends Component {

  state = {
    yesnoOpts: [{
        label: 'No',
        value: 'NO',
        color: Colors.snow,
      },
      {
        label: 'Yes',
        value: 'YES',
        color: Colors.snow,
      },

    ],
    yesnoOpts2: [{
      label: 'No',
      value: 'NO',
      color: Colors.snow,
    },
    {
      label: 'Yes',
      value: 'YES',
      color: Colors.snow,
    },

  ],
    profOpts: [{
        label: 'Medical Doctor',
        value: 'doctor',
        color: Colors.snow,
      },
      {
        label: 'Medical Nurse',
        value: 'nurse',
        color: Colors.snow,
      },{
        label: 'Rescue',
        value: 'rescue',
        color: Colors.snow,
      },{
        label: 'Para Medic',
        value: 'para-medic',
        color: Colors.snow,
      },
    ],
    isDoctor: false,
    form: {
      name: "",
      phone: "",
      password: "",
      country: "",
      lang: "",
      is_doctor: "",
      prof: "",
      is_volunteer: 0,
      email:""
    }
  };

  onSelectDoctor = data => {
    if (data.value === "YES"){
      this.setState({isDoctor: true , form: {...this.state.form , is_doctor: true}})
    }else{
      this.setState({isDoctor: false, form: {...this.state.form , is_doctor: false}})
    }
  };

  onSelectProf = data => {
    this.setState({form: {...this.state.form , prof: data}})
  }
  onSelectVul = data => {
    this.setState({form: {...this.state.form , is_volunteer: data}})
  }

  isAttempting = false
  constructor(props){
    super(props);
    this.isAttempting = false
  }



  initRegister(){
    this.props.register(this.state.form);
  }

  componentDidMount(){

  }

  render () {
    const { fetching } = this.props
    const editable = !fetching
    const textInputStyle = editable ? styles.textInput : styles.textInputReadonly

    return (
      <ScrollView style={[styles.container , styles.paddedPage]}>
        <KeyboardAvoidingView
        behavior='position'>

          <View style={styles.formInput}>
            <UiFieldLabel text="Full Name" image={Images.ic_person_outline}/>
            <UiField
              name={"fullname"}
              returnKeyType='next'
              autoCorrect={false}
              ref='fullname'
              onChange={(v) => this.setState({form: {...this.state.form , name: v}})}
              textContentType={"name"}
              placeholder="Enter your full name, Ex: John Doe"
            />
          </View>



          <View style={styles.formInput}>
            <UiFieldLabel text="Phone Number" image={Images.ic_phone}/>
            <UiField
              name={"phone"}
              returnKeyType='next'
              autoCorrect={false}
              ref='phone'
              onChange={(v) => this.setState({form: {...this.state.form , phone: v}})}
              textContentType={"telephoneNumber"}
              keyboardType={"numeric"}
              placeholder="- - -   - - - -   - - - -"
            />
          </View>

          <View style={styles.formInput}>
            <UiFieldLabel text="Email Address" image={Images.ic_email}/>
            <UiField
              name={"email"}
              returnKeyType='next'
              autoCorrect={false}
              keyboardType={'email-address'}
              textContentType={"email"}
              onChange={(v) => this.setState({form: {...this.state.form , email: v}})}
              ref='email'
              placeholder="John.doe@example.com"
            />
            </View>


          <View style={styles.formInput}>
            <UiFieldLabel text="Password" image={Images.ic_lock}/>
            <UiField
              name={"password"}
              returnKeyType='next'
              autoCorrect={false}
              keyboardType={'visible-password'}
              textContentType={"password"}
              onChange={(v) => this.setState({form: {...this.state.form , password: v}})}
              ref='password'
              placeholder="********"
            />
          </View>

          <View style={styles.formInput}>
            <UiFieldLabel text="Country" image={Images.ic_world}/>
            <UiField
              name={"country"}
              isPicker={true}
              ref='country'
              selectChange={(v) => {
                this.setState({form: {...this.state.form , country: v}})
              }}
            >
            {configs.CONF_CONTRIES.map((object) => <Option key={object.toString()} value={object}>{object}</Option>)}
            </UiField>
          </View>

          <View style={styles.formInput}>
            <UiFieldLabel text="Langauge" image={Images.ic_lang}/>
            <UiField
              name={"language"}
              isPicker={true}
              ref='langauge'
              selectChange={(v) => {
                this.setState({form: {...this.state.form , lang: v}})
              }}
            >
            {configs.CONF_LANGS.map((object) => <Option key={object.toString()} value={object}>{object}</Option>)}
            </UiField>
          </View>

          <View style={styles.formInput}>
            <UiFieldLabel text="Are you health care professional"/>
            <RadioGroup radioButtons={this.state.yesnoOpts} onPress={this.onSelectDoctor} flexDirection='row'  />
          </View>

          {this.state.isDoctor &&
          <View>

            <View style={styles.formInput}>
              <UiFieldLabel text="Profession:"/>
              <RadioGroup radioButtons={this.state.profOpts} onPress={this.onSelectProf}   />
            </View>

            <View style={styles.formInput}>
              <UiFieldLabel text="Are You Ready To Volunteer During Hajj?"/>
              <RadioGroup radioButtons={this.state.yesnoOpts2} onPress={this.onSelectVul}   />
            </View>
          </View>
          }
        </KeyboardAvoidingView>
        <RoundedButton
          text='REGISTER'
          onPress={() => {
            this.initRegister();
          }}
          icon={Images.ic_person_add}
          transparent={true}
        />
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fetching: true
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    register: (data) => dispatch(RegisterAction.registerFormRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)
