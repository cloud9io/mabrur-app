import React , {Component} from 'react'
import { ScrollView, View, KeyboardAvoidingView , TouchableOpacity , Text} from 'react-native'
import { connect } from 'react-redux'
import { Option} from "react-native-chooser";
import RadioGroup from 'react-native-radio-buttons-group';
import UiField from '../Components/UiField';
import UiFieldLabel from '../Components/UiFieldLabel';
import { Images, Colors } from '../Themes';
import configs from '../Config/AppConfig';
import RoundedButton from '../Components/RoundedButton'
import { Actions } from 'react-native-router-flux';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LoginScreenStyle'

class LoginScreen extends Component {

  state = {

    form: {
      name: "",
      phone: "",
      password: "",
      country: "",
      lang: "",
      is_doctor: "",
      prof: "",
      is_volunteer: 0,
      email:""
    }
  };

  onSelectDoctor = data => {
    if (data.value === "YES"){
      this.setState({isDoctor: true , form: {...this.state.form , is_doctor: true}})
    }else{
      this.setState({isDoctor: false, form: {...this.state.form , is_doctor: false}})
    }
  };

  onSelectProf = data => {
    this.setState({form: {...this.state.form , prof: data}})
  }
  onSelectVul = data => {
    this.setState({form: {...this.state.form , is_volunteer: data}})
  }

  isAttempting = false
  constructor(props){
    super(props);
    this.isAttempting = false
  }



  initRegister(){
      Actions.home()
  }

  componentDidMount(){

  }

  render () {
    const { fetching } = this.props
    const editable = !fetching
    const textInputStyle = editable ? styles.textInput : styles.textInputReadonly

    return (
      <ScrollView style={[styles.container , styles.paddedPage]}>
        <KeyboardAvoidingView
        behavior='position'>






          <View style={styles.formInput}>
            <UiFieldLabel text="Email Address" image={Images.ic_email}/>
            <UiField
              name={"email"}
              returnKeyType='next'
              autoCorrect={false}
              keyboardType={'email-address'}
              textContentType={"email"}
              onChange={(v) => this.setState({form: {...this.state.form , email: v}})}
              ref='email'
              placeholder="John.doe@example.com"
            />
            </View>


          <View style={styles.formInput}>
            <UiFieldLabel text="Password" image={Images.ic_lock}/>
            <UiField
              name={"password"}
              returnKeyType='next'
              autoCorrect={false}
              keyboardType={'visible-password'}
              textContentType={"password"}
              onChange={(v) => this.setState({form: {...this.state.form , password: v}})}
              ref='password'
              placeholder="********"
            />
          </View>


        <RoundedButton
          text='LOGIN'
          onPress={() => {
            this.initRegister();
          }}
          icon={Images.ic_person_outline}
          transparent={true}
        />
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
